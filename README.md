## SOURCE CODE REPOSITORY
https://bitbucket.org/yalonsob/simple_database

## REQUIREMENTS
`java version "1.8.0_91"`

## HOW TO EXECUTE THE PROGRAM

- In the console navigate to `dist` folder
- Once inside `dist` folder for running the program interactively write: `java com.turo.Main`
- For running the program passing a file of commands write: `java com.turo.Main < commands.txt`

## THOUGHT PROCESS

I decided to develop the program using TDD to make sure any changes made later in the code wouldn't affect the program functionality.

I started solving the basic commands, that is: SET, GET, UNSET and NUMEQUALTO.

I decided to first focus on the them without considering transactions.

When I had to introduce transactions I decided to focus on accomplishing functionality even if the performance was not meeting the requirements.

Once the program was working correctly with the complete functionality I worked on the performance requirements.

I had to modify several things inside the code, including new data structures. At this point, the tests I had written were very helpful to make sure everything was still working as expected.

Even though it was not necessary I extracted a Database Interface which has an implementation of InMemoryDatabase with the specifications of this problem.

That allows me to introduce new implementations of the persistent mechanism without changing the SimpleDataBase. For instance, we could introduce an InFileDatabase to store the Database in disk instead of memory.

## TIME COMPLEXITY ANALYSIS
- I'm considering the vast majority of transactions will only update a small number of variables M (M << N), as it is said in the problem statement.
- M = number of operations in a transaction.
- At any given point of time I'm considering worst case scenario for number of transactions is T
- T = number of open transactions.
- N = number of keys.

### Operations
- SET: O(1)
- GET: O(1)
- UNSET: O(1)
- NUMEQUALTO: O(N)
- ROLLBACK: O(M)
- COMMIT: O(T * M)


## INTERNAL DATA STRUCTURE OF THE IN-MEMORY DATABASE
```
            TRANSACTIONS
            ------------
           T0    T1    T2
K1 ->  [  V1.0  V1.1  V1.2  ]

K2 ->  [  V2.0  V2.1  V2.2  ]    <- CHANGES OF VALUES FOR EACH KEY IN TRANSACTIONS

K3 ->  [  V3.0  V3.1  V3.2  ]
```