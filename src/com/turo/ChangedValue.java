package com.turo;

class ChangedValue {
    private String value;
    private int transaction;

    public ChangedValue(String value, int transaction) {
        this.value = value;
        this.transaction = transaction;
    }

    public String getValue() {
        return value;
    }

    public int getTransaction() {
        return transaction;
    }
}
