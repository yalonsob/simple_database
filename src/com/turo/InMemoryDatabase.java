package com.turo;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class InMemoryDatabase implements Database {

    private static final int NON_TRANSACTIONAL = 0;
    private Stack<HashMap<String, String>> transactions = new Stack<>();
    private HashMap<String, ChangeSet> changesByKey = new HashMap<>();

    @Override
    public String get(String key) {
        if(!changesByKey.containsKey(key))
            return null;
        return changesByKey.get(key).lastValue();
    }

    @Override
    public void set(String key, String value) {
        if(activeTransaction())
            setTransactionalValue(key, value);
        else
            setNonTransactionalValue(key, value);
    }

    private void setTransactionalValue(String key, String value){
        currentTransactionStorage().put(key, value);
        if(!changesByKey.containsKey(key))
            changesByKey.put(key, new ChangeSet());
        changesByKey.get(key).addChange(new ChangedValue(value, currentTransactionId()));
    }

    private void setNonTransactionalValue(String key, String value){
        if(!changesByKey.containsKey(key))
            changesByKey.put(key, new ChangeSet());
        if(value == null){
            changesByKey.remove(key);
        }else{
            changesByKey.get(key).addChange(new ChangedValue(value, NON_TRANSACTIONAL));
        }
    }

    private HashMap<String, String> currentTransactionStorage() {
        return transactions.peek();
    }

    private int currentTransactionId() {
        return transactions.size();
    }

    @Override
    public void unset(String key) {
        set(key, null);
    }

    @Override
    public int numberOfKeysEqualTo(String value) {
        int count = 0;
        for(ChangeSet changes : changesByKey.values())
            if (changes.lastValue() != null && changes.lastValue().equals(value))
                count++;
        return count;
    }

    @Override
    public void beginTransaction() {
        transactions.add(new HashMap<>());
    }

    @Override
    public boolean rollback() {
        if(!activeTransaction())
            return false;
        discardChangesInLastTransaction();
        return true;
    }

    private void discardChangesInLastTransaction() {
        for(String key : currentTransactionStorage().keySet())
            discardLastChangeForKey(key);
        transactions.pop();
    }

    private void discardLastChangeForKey(String key) {
        ChangeSet changeSet = changesByKey.get(key);
        changeSet.discardLastChange();
        if(changeSet.empty())
            changesByKey.remove(key);
    }

    @Override
    public boolean commit() {
        if(!activeTransaction())
            return false;
        applyChangesInAllTransactions();
        return true;
    }

    private void applyChangesInAllTransactions() {
        HashMap<String, String> changesToApply = changesToApply();
        for (Map.Entry<String, String> entry : changesToApply.entrySet()) {
            changesByKey.remove(entry.getKey());
            setNonTransactionalValue(entry.getKey(), entry.getValue());
        }
        transactions = new Stack<>();
    }

    private HashMap<String, String> changesToApply() {
        HashMap<String, String> changesToApply = new HashMap<>();
        for(HashMap<String, String> transaction : transactions)
            changesToApply.putAll(transaction);
        return changesToApply;
    }

    private boolean activeTransaction() {
        return numberOfOpenTransactions() > 0;
    }

    private int numberOfOpenTransactions(){
        return transactions.size();
    }

}
