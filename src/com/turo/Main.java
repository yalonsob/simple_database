package com.turo;

import com.sun.corba.se.impl.orbutil.ObjectUtility;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.sql.PreparedStatement;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        run(System.in, System.out);
    }

    public static void run(InputStream inputStream, OutputStream outputStream) {
        PrintStream printStream = new PrintStream(outputStream);
        SimpleDatabase simpleDatabase = new SimpleDatabase(new InMemoryDatabase());
        Scanner scanner = new Scanner(inputStream);
        String command = scanner.nextLine();
        while (!command.equals("END")){
            String output = simpleDatabase.execute(command);
            if(!output.equals(""))
                printStream.println(output);
            command = scanner.nextLine();
        }
    }
}
