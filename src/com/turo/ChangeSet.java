package com.turo;


import java.util.Stack;

public class ChangeSet {
    private Stack<ChangedValue> changes = new Stack<>();

    public void addChange(ChangedValue value){
        if(!changes.empty() && changes.peek().getTransaction() == value.getTransaction())
            changes.pop();
        changes.push(value);
    }

    public String lastValue(){
        if(changes.empty())
            return null;
        return changes.peek().getValue();
    }

    public void discardLastChange() {
        changes.pop();
    }

    public boolean empty(){
        return changes.empty();
    }
}
