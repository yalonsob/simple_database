package com.turo;

/**
 * Created by samsung on 16/07/2016.
 */
public interface Database {
    String get(String key);

    void set(String key, String value);

    void unset(String key);

    int numberOfKeysEqualTo(String value);

    void beginTransaction();

    boolean rollback();

    boolean commit();
}
