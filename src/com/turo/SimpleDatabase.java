package com.turo;

public class SimpleDatabase {
    private Database database;

    public SimpleDatabase(Database database){
        this.database = database;
    }

    public String execute(String command){
        String[] commandArgs = command.split("\\s+");
        if(command.startsWith("GET"))
            return executeGet(commandArgs);
        if(command.startsWith("SET"))
            return executeSet(commandArgs);
        if(command.startsWith("UNSET"))
            return executeUnset(commandArgs);
        if(command.startsWith("NUMEQUALTO"))
            return executeNumEqualsTo(commandArgs);
        if(command.startsWith("BEGIN"))
            return executeBegin(commandArgs);
        if(command.startsWith("COMMIT"))
            return executeCommit(commandArgs);
        if(command.startsWith("ROLLBACK"))
            return executeRollback(commandArgs);
        return "";
    }

    private String executeRollback(String[] commandArgs) {
        if(database.rollback())
            return "";
        return "NO TRANSACTION";
    }

    private String executeCommit(String[] commandArgs) {
        if(database.commit())
            return "";
        return "NO TRANSACTION";
    }

    private String executeBegin(String[] commandArgs) {
        database.beginTransaction();
        return "";
    }

    private String executeNumEqualsTo(String[] commandArgs) {
        return Integer.toString(database.numberOfKeysEqualTo(commandArgs[1]));
    }

    private String executeUnset(String[] commandArgs) {
        database.unset(commandArgs[1]);
        return "";
    }

    private String executeSet(String[] commandArgs) {
        database.set(commandArgs[1], commandArgs[2]);
        return "";
    }

    private String executeGet(String[] commandArgs) {
        String result = database.get(commandArgs[1]);
        return result == null
                ? "NULL"
                : result;
    }

}
