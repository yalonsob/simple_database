package com.turo.tests;


import com.turo.Main;
import org.junit.Test;

import java.io.InputStream;
import java.io.OutputStream;

import static org.junit.Assert.assertEquals;

public class MainTest {
    @Test
    public void endToEnd1(){
        String output = execute(String.join("\n",
                "BEGIN",
                "SET a 10",
                "GET a",
                "BEGIN",
                "SET a 20",
                "GET a",
                "ROLLBACK",
                "GET a",
                "ROLLBACK",
                "GET a",
                "END"
        ));
        assertEquals(String.join("\r\n",
                "10",
                "20",
                "10",
                "NULL\r\n"
        ), output);
    }

    @Test
    public void endToEnd2(){
        String output = execute(String.join("\n",
                "BEGIN",
                "SET a 30",
                "BEGIN",
                "SET a 40",
                "COMMIT",
                "GET a",
                "ROLLBACK",
                "COMMIT",
                "END"
        ));
        assertEquals(String.join("\r\n",
                "40",
                "NO TRANSACTION",
                "NO TRANSACTION\r\n"
        ), output);
    }

    @Test
    public void endToEnd3(){
        String output = execute(String.join("\n",
                "SET a 50",
                "BEGIN",
                "GET a",
                "SET a 60",
                "BEGIN",
                "UNSET a",
                "GET a",
                "ROLLBACK",
                "GET a",
                "COMMIT",
                "GET a",
                "END"
        ));
        assertEquals(String.join("\r\n",
                "50",
                "NULL",
                "60",
                "60\r\n"
        ), output);
    }

    @Test
    public void endToEnd4(){
        String output = execute(String.join("\n",
                "SET a 10",
                "BEGIN",
                "NUMEQUALTO 10",
                "BEGIN",
                "UNSET a",
                "NUMEQUALTO 10",
                "ROLLBACK",
                "NUMEQUALTO 10",
                "COMMIT",
                "END"
        ));
        assertEquals(String.join("\r\n",
                "1",
                "0",
                "1\r\n"
        ), output);
    }

    private String execute(String input){
        InputStream stringStream = new java.io.ByteArrayInputStream(input.getBytes());
        OutputStream outputStream = new java.io.ByteArrayOutputStream();
        Main.run(stringStream, outputStream);
        return outputStream.toString();
    }
}
