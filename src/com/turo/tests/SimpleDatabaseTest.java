package com.turo.tests;

import com.turo.InMemoryDatabase;
import com.turo.SimpleDatabase;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.IncludeCategories;

import static junit.framework.TestCase.*;

public class SimpleDatabaseTest {

    private SimpleDatabase database;

    @Before
    public void setUp() throws Exception {
        database = new SimpleDatabase(new InMemoryDatabase());
    }

    @Test
    public void whenTheDatabaseIsEmptyItGetsNullForAnyKey(){
        assertEquals("NULL", database.execute("GET any_key"));
    }

    @Test
    public void settingAndGettingKeys(){
        database.execute("SET key_1 1");
        database.execute("SET key_2 2");
        database.execute("SET key_3 3");

        assertEquals("1", database.execute("GET key_1"));
        assertEquals("2", database.execute("GET key_2"));
        assertEquals("3", database.execute("GET key_3"));
    }

    @Test
    public void whenKeysAreReplacedItKeepsTheLastValue(){
        database.execute("SET key 1");
        database.execute("SET key 2");

        assertEquals("2", database.execute("GET key"));
    }

    @Test
    public void whenAKeyIsUnsetItRetrievesANullValue(){
        database.execute("SET key 1");
        database.execute("UNSET key");

        assertEquals("NULL", database.execute("GET key"));
    }

    @Test
    public void canRetrieveTheNumberOfKeysThatHaveAValue() {
        database.execute("SET key_1 1");
        database.execute("SET key_2 1");
        database.execute("SET key_3 1");
        database.execute("SET key_4 2");
        database.execute("SET key_3 2");
        database.execute("UNSET key_2");
        database.execute("UNSET non_existent_key");

        assertEquals("1", database.execute("NUMEQUALTO 1"));
    }

    @Test
    public void keepsValuesWithinTransactionContext(){
        database.execute("BEGIN");
        database.execute("SET key_1 1");
        database.execute("SET key_2 2");
        database.execute("UNSET key_2");

        assertEquals("1", database.execute("GET key_1"));
        assertEquals("NULL", database.execute("GET key_2"));
    }

    @Test
    public void whenNoTransactionRollbackFails(){
        assertEquals("NO TRANSACTION", database.execute("ROLLBACK"));
    }

    @Test
    public void whenTransactionStartedRollbackSucceeds(){
        database.execute("BEGIN");
        assertEquals("", database.execute("ROLLBACK"));
    }

    @Test
    public void whenMoreRollbacksThanTransactionsItFails(){
        database.execute("BEGIN");

        assertEquals("", database.execute("ROLLBACK"));
        assertEquals("NO TRANSACTION", database.execute("ROLLBACK"));
    }

    @Test
    public void whenTransactionsAreEqualToRollbacksItSucceeds(){
        database.execute("BEGIN");
        database.execute("BEGIN");

        assertEquals("", database.execute("ROLLBACK"));
        assertEquals("", database.execute("ROLLBACK"));
    }

    @Test
    public void revertsValuesAfterRollback(){
        database.execute("SET key_1 1");
        database.execute("SET key_2 2");
        database.execute("BEGIN");
        database.execute("SET key_2 3");
        database.execute("UNSET key_1");
        database.execute("SET key_3 3");

        database.execute("ROLLBACK");

        assertEquals("1", database.execute("GET key_1"));
        assertEquals("2", database.execute("GET key_2"));
        assertEquals("NULL", database.execute("GET key_3"));
    }

    @Test
    public void nestedRollbacks(){
        database.execute("SET key 1");
        database.execute("BEGIN");
        database.execute("SET key 2");
        database.execute("BEGIN");
        database.execute("SET key 3");

        database.execute("ROLLBACK");

        assertEquals("2", database.execute("GET key"));
    }

    @Test
    public void whenNoTransactionCommitFails(){
        assertEquals("NO TRANSACTION", database.execute("COMMIT"));
    }

    @Test
    public void whenTransactionStartedCommitSucceeds(){
        database.execute("BEGIN");
        assertEquals("", database.execute("COMMIT"));
    }

    @Test
    public void afterCommitAllTransactionsAreFinished(){
        database.execute("BEGIN");
        database.execute("BEGIN");

        assertEquals("", database.execute("COMMIT"));
        assertEquals("NO TRANSACTION", database.execute("COMMIT"));
    }

    @Test
    public void commitsAllOperationsInTransactions(){
        database.execute("SET key_1 1");
        database.execute("BEGIN");
        database.execute("SET key_2 2");
        database.execute("BEGIN");
        database.execute("UNSET key_1");
        database.execute("SET key_2 3");
        database.execute("BEGIN");
        database.execute("SET key_3 3");

        database.execute("COMMIT");

        assertEquals("NULL", database.execute("GET key_1"));
        assertEquals("3", database.execute("GET key_2"));
        assertEquals("3", database.execute("GET key_3"));
    }
}
